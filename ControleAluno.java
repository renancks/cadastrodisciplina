import java.util.ArrayList;
public class ControleAluno(){

	//atributos 
	private ArrayList<Aluno> listaAluno;
	
	//construtor
	public ControleAluno(){
		listaAluno = new ArrayList<Aluno>();
	}
	//Metodos 
	public String adicionar (Aluno umAluno){
		String mensagem = "Aluno Adicionado";
		listaAluno.add(umAluno);
		return mensagem;
	}
	public String remover(Aluno umAluno){
		String mensagem = "Aluno Removido";
		listaAluno.remove(umAluno);
		return mensagem;
	}
	public Aluno pesquisarNome(String umNome){
		for(Aluno umAluno : listaAluno){
			if (umAluno.getNome().equalsIgnoreCase(umNome))return umAluno;
		}//fim do for
		return null;
	}
	public Aluno pesquisarMatricula(String umaMatricula){
		for(Aluno umaMatricula : listaAluno){
			if(umAluno.getMatricula().equalsIgnoreCase(umaMatricula)) return umAluno;
		}
		return null;
	}
	public void exibirLista(){
		for(Aluno umAluno : listaAluno){
			System.out.println(umAluno.getNome());
		}
	}

}//fim da classe ControleAluno
