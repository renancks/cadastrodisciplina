import java.util.ArrayList;
public class ControleDisciplina{
	//atributos
	private ArrayList<Disciplina> listaDisciplina;
	//Construtor
	public ControleDisciplina(){
		listaDisciplina = new ArrayList<Disciplina>();
	}
	//Metodos
	public String Adicionar(Disciplina umaDisciplina){
		String mensagem = "Disciplina Adicionada";
		listaDisciplina.add(umaDisciplina);
		return mensagem;
	}
	public String remover(Disciplina umaDisciplina){
		String mensagem ="Disciplina Removida";
		listaDisciplina.remove(umaDisciplina);
		return mensagem;
	}
	public Disciplina pesquisarNomeDisciplina(String umNomeDisciplina){
		for (Disciplina umaDisciplina : listaDisciplina){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;	
		} 
		return null;
	}
	public Disciplina pesquisarCodigoDisciplina(String umCodigoDisciplina){
		for (Disciplina umaDisciplina : listaDisciplina){
			if(umaDisciplina.getCodigoDisciplina().equalsIgnoreCase(umCodigoDisciplina)) return umaDisciplina;
		}
		return null;
	}
	public void exibirDisciplina() {
		for (Disciplina umaDisciplina : listaDisciplina){
			System.out.println(umaDisciplina.getNomeDisciplina());
		}
	}
	
}//fim da classe Disciplina
