public class Aluno(){
	//atributos
		private String nome;
		private String matricula;
		
		
		//construtores
		public Aluno(){
		}
		public Aluno(String nome){
			this.nome = nome;
		}
		public Aluno(String nome, String matricula){
			this.nome = nome;
			this.matricula = matricula;
		}
		
		//Metodos 
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getNome(){
			return nome;
		}
		public void setMatricula(String matricula){
			this.matricula = matricula;
		}
		public String getMatricula(){
			return matricula;
		}

}//fim da classe Alunos
