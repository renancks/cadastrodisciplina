public class Disciplina (){
	//atributos
	private String nomeDisciplina;
	private String codigoDisciplina;
	private String turma;
	
	//Construtor
	public Disciplina(){
	}
	public Disciplina (String nomeDisciplina){
		this.nomeDisciplina = nomeDisciplina;
	}
	public Disciplina (String nomeDisciplina, String codigoDisciplina, String turma){
		this.nomeDisciplina = nomeDisciplina;
		this.codigoDisciplina = codigoDisciplina;
		this.turma = turma;
	}
	//metodo
	public void setNomeDisciplina(String nomeDisciplina){
		this.nomeDisciplina = nomeDisciplina;
	}
	public String getNomeDisciplina(){
		return nomeDisciplina;
	}
	public void setCodigoDisciplina(String codigoDisciplina){
		this.codigoDisciplina = codigoDisciplina;
	}
	public String getCodigoDisciplina(){
	 	return codigoDisciplina;
	} 

}//fim da classe Disciplina
